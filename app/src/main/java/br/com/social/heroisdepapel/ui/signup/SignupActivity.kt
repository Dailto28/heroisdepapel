package br.com.social.heroisdepapel.ui.signup

import android.Manifest.permission.READ_PHONE_STATE
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.telephony.TelephonyManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import br.com.social.heroisdepapel.R
import br.com.social.heroisdepapel.databinding.ActivitySignupBinding
import br.com.social.heroisdepapel.domain.common.Result
import br.com.social.heroisdepapel.domain.usecases.GetUserByImeiUseCase
import br.com.social.heroisdepapel.domain.usecases.InsertUserUseCase
import br.com.social.heroisdepapel.ui.maps.MapsActivity
import org.koin.android.ext.android.inject
import java.util.regex.Pattern

/**
 * Realiza o cadastro inicial do usuario para que possa ter acesso aos recursos
 * da aplicacao
 */
class SignupActivity : AppCompatActivity() {

    /**
     * Aqui reflete o mapeamento dos campos do xml de layout
     */
    private lateinit var binding: ActivitySignupBinding

    val insertUserUseCase: InsertUserUseCase by inject()
    val getUserByImeiUseCase: GetUserByImeiUseCase by inject()

    var cpfPattern = Pattern.compile("[0-9][0-9][0-9]\\.[0-9][0-9][0-9]\\.[0-9][0-9][0-9]-[0-9][0-9]")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup)
        binding.viewModel = SignupViewModel()

        bindingAddOnPropertyChangedCallback()
    }

    override fun onStart() {
        super.onStart()

        checkAndSetImeiAndPhoneNumber()
    }

    /**
     * Prepara os bindings de comandos de mudanca
     */
    private fun bindingAddOnPropertyChangedCallback() {

        binding.viewModel!!.isSignupDone.addOnPropertyChangedCallback(object: Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (binding.viewModel!!.isSignupDone.get()) {

                    var params = InsertUserUseCase.Params(
                        0,
                        binding.viewModel!!.phone.get(),
                        binding.viewModel!!.name.get(),
                        binding.viewModel!!.cpf.get(),
                        binding.viewModel!!.email.get(),
                        binding.viewModel!!.imei.get(),
                        binding.viewModel!!.password.get()
                    )

                    insertUserUseCase.execute(params)

                    val intent = Intent(this@SignupActivity, MapsActivity::class.java)
                    startActivity(intent)
                }
            }
        })

        binding.viewModel!!.isError.addOnPropertyChangedCallback(object: Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (binding.viewModel!!.isError.get()) {

                    if (!android.util.Patterns.EMAIL_ADDRESS.matcher(binding.viewModel!!.email.get()).matches()) {
                        binding.emailEditText.error = "Email deve ser preenchido corretamente, por exemplo teste@teste.com"
                    } else {
                        binding.emailEditText.error = null
                    }

                    if (!cpfPattern.matcher(binding.viewModel!!.cpf.get()).matches()) {
                        binding.cpfEditText.error = "CPF deve seguir conforme o padrao 000.000.000-04"
                    } else {
                        binding.cpfEditText.error = null
                    }

                    if (binding.viewModel!!.phone.get().isNullOrEmpty() || binding.viewModel!!.imei.get().isNullOrEmpty()) {
                        alertCheckReadPhoneStatePermission()
                    } else {
                        Toast.makeText(this@SignupActivity, "Precisa de todos os campos preenchidos", Toast.LENGTH_LONG).show()
                    }
                    binding.viewModel!!.isError.set(false)
                }
            }
        })
    }

    /**
     * Exibe o alert com o caso a permissao seja requisitada
     */
    private fun alertCheckReadPhoneStatePermission() {
        if (checkSelfPermission(READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
            val alertBuilder = AlertDialog.Builder(this@SignupActivity, R.style.AlertStyle)
            alertBuilder.setTitle("Requisição de permissão")
            alertBuilder.setMessage("O aplicativo precisará pegar o numero de telefone para poder dar continuidade a criação de conta")
            alertBuilder.setPositiveButton("Permitir") { dialog, which ->
                run {
                    requestPermissions(arrayOf(READ_PHONE_STATE), PERMISSION_READ_PHONE_STATE)
                }
            }
            alertBuilder.show()
        } else {
            checkAndSetImeiAndPhoneNumber()
        }
    }

    /**
     * Verifica e seta os dados base de identificacao do aparelho
     */
    private fun checkAndSetImeiAndPhoneNumber() {
        if (checkSelfPermission(READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val telephonySystemService = applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            binding.viewModel!!.phone.set(telephonySystemService.line1Number)
            binding.viewModel!!.imei.set(telephonySystemService.imei)

            val execute = getUserByImeiUseCase.execute(GetUserByImeiUseCase.Params.forUser(telephonySystemService.imei))

            if (execute is Result.Success<*>) {
                val intent = Intent(this@SignupActivity, MapsActivity::class.java)
                startActivity(intent)
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_READ_PHONE_STATE) {
            checkAndSetImeiAndPhoneNumber()
        }
    }

    companion object {

        /**
         * Constante de requicao para verificar se os dados foram validados
         */
        private const val PERMISSION_READ_PHONE_STATE = 1_234
    }
}
