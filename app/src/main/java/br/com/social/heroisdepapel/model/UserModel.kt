package br.com.social.heroisdepapel.model

/**
 * Usuario para poder ser mapeado no sistema
 */
data class UserModel (
    var id: Long,
    var phone: String,
    var name: String,
    var cpf: String,
    var email: String,
    var imei: String,
    var password: String
)