package br.com.social.heroisdepapel.ui.maps

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import br.com.social.heroisdepapel.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, Drawer.OnDrawerItemClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var mDrawer: Drawer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val drawerBuilder = DrawerBuilder()
            .withActivity(this)
            .inflateMenu(R.menu.menu_drawer)
            .withOnDrawerItemClickListener(this)

        mDrawer = drawerBuilder.build()

        menuIv.setOnClickListener {
            if (mDrawer.isDrawerOpen) {
                mDrawer.closeDrawer()
            } else {
                mDrawer.openDrawer()
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    override fun onItemClick(view: View?, position: Int, drawerItem: IDrawerItem<*, *>?): Boolean {
        val alertBuilder = AlertDialog.Builder(this, R.style.AlertStyle)
        alertBuilder.setTitle("Recurso ainda em desenvolvimento")
        alertBuilder.setMessage("No sistema implementamos apenas o caso de uso de acesso inicial")
        alertBuilder.setPositiveButton("Ok") { dialog, which -> dialog.dismiss() }
        alertBuilder.show()

        return true
    }
}
