package br.com.social.heroisdepapel

import android.app.Application
import br.com.social.heroisdepapel.data.di.dataMapper
import br.com.social.heroisdepapel.data.di.dataRepositoryModule
import br.com.social.heroisdepapel.data.di.dataUseCaseModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Serve para poder utilizar as configuracoes para
 * a aplicacao conforme a necessidade
 */
class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AppApplication)
            modules(
                listOf(
                    dataMapper,
                    dataRepositoryModule,
                    dataUseCaseModule
                )
            )
        }
    }
}