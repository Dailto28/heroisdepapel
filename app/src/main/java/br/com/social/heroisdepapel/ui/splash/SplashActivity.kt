package br.com.social.heroisdepapel.ui.splash

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.telephony.TelephonyManager
import androidx.appcompat.app.AlertDialog
import br.com.social.heroisdepapel.R
import br.com.social.heroisdepapel.domain.common.Result
import br.com.social.heroisdepapel.domain.usecases.GetUserByImeiUseCase
import br.com.social.heroisdepapel.ui.maps.MapsActivity
import br.com.social.heroisdepapel.ui.signup.SignupActivity
import org.koin.android.ext.android.inject

class SplashActivity : AppCompatActivity() {

    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable
    private val getUserByImeiUseCase: GetUserByImeiUseCase by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mHandler = Handler()

        mRunnable = Runnable {
            alertCheckReadPhoneStatePermission()
        }
    }

    override fun onStart() {
        super.onStart()

        mHandler.postDelayed(mRunnable, 3000)
    }

    /**
     * Exibe o alert com o caso a permissao seja requisitada
     */
    private fun alertCheckReadPhoneStatePermission() {
        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
            val alertBuilder = AlertDialog.Builder(this@SplashActivity, R.style.AlertStyle)
            alertBuilder.setTitle("Requisição de permissão")
            alertBuilder.setCancelable(false)
            alertBuilder.setMessage("O aplicativo precisará pegar o numero de telefone para poder dar continuidade a criação de conta")
            alertBuilder.setPositiveButton("Permitir") { dialog, which ->
                run {
                    requestPermissions(arrayOf(Manifest.permission.READ_PHONE_STATE),
                        PERMISSION_READ_PHONE_STATE
                    )
                }
            }
            alertBuilder.show()
        } else {
            checkAndSetImeiAndPhoneNumber()
        }
    }

    /**
     * Verifica e seta os dados base de identificacao do aparelho
     */
    private fun checkAndSetImeiAndPhoneNumber() {
        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            val telephonySystemService = applicationContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            val execute = getUserByImeiUseCase.execute(GetUserByImeiUseCase.Params.forUser(telephonySystemService.imei))

            if (execute is Result.Success<*>) {
                val intent = Intent(this, MapsActivity::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this, SignupActivity::class.java)
                startActivity(intent)
            }
        } else {
            mHandler.postDelayed(mRunnable, 3000)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_READ_PHONE_STATE) {
            checkAndSetImeiAndPhoneNumber()
        }
    }

    companion object {

        /**
         * Constante de requicao para verificar se os dados foram validados
         */
        private const val PERMISSION_READ_PHONE_STATE = 1_234
    }
}
