package br.com.social.heroisdepapel.ui.signup

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField

/**
 * Serve para realizar a captura das informacoes do usuario para assim podermos
 * re
 */
class SignupViewModel {

    var phone: ObservableField<String> = ObservableField("")
    var imei: ObservableField<String> = ObservableField("")
    var name: ObservableField<String> = ObservableField("")
    var cpf: ObservableField<String> = ObservableField("")
    var email: ObservableField<String> = ObservableField("")
    var password: ObservableField<String> = ObservableField("")
    var confirmPassword: ObservableField<String> = ObservableField("")

    /**
     * Serve para verificar se pode ser salvo ou nao
     */
    var isSignupDone: ObservableBoolean = ObservableBoolean(false)
    var isError: ObservableBoolean = ObservableBoolean(false)

    fun onClickConfirm() {
        var mPhone = phone.get()
        var mImei = imei.get()
        var mName = name.get()
        var mCpf = cpf.get()
        var mEmail = email.get()
        var mPassword = password.get()
        var mConfirmPassword = confirmPassword.get()

        var error = false

        if (mPhone.equals("")) {
            error = true
        }

        if (mImei.equals("")) {
            error = true
        }

        if (mName.equals("")) {
            error = true
        }

        if (mCpf.equals("")) {
            error = true
        }

        if (mEmail.equals("") || !android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            error = true
        }

        if (mPassword.equals("")) {
            error = true
        }

        if (mConfirmPassword.equals("")) {
            error = true
        }

        if (!mConfirmPassword.equals(mPassword)) {
            error = true
        }

        if (error) {
            isError.set(true)
            return
        }

        isSignupDone.set(true)
    }
}
