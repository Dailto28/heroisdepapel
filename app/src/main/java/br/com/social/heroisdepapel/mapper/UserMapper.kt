package br.com.social.heroisdepapel.mapper

import br.com.social.heroisdepapel.domain.common.Mapper
import br.com.social.heroisdepapel.domain.model.User
import br.com.social.heroisdepapel.model.UserModel

/**
 * Serve para o processo de conversao de entidade para um formato reconhecivel pelo modulo
 */
class UserMapper : Mapper<UserModel, User> {

    override fun transform(from: UserModel): User {
        var result = User()
        result.id = from.id
        result.cpf = from.cpf
        result.email = from.email
        result.imei = from.imei
        result.name = from.name
        result.password = from.password
        result.phone = from.phone
        return result
    }

    override fun reverse(from: User): UserModel = UserModel(
        id = from.id,
        cpf = from.cpf,
        email = from.email,
        imei = from.imei,
        name = from.name,
        password = from.password,
        phone = from.phone
    )
}