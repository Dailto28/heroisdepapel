package br.com.social.heroisdepapel.ui.signup

import org.junit.Assert
import org.junit.Before
import org.junit.Test

class SignupViewModelTest {

    private lateinit var mSignupViewModel: SignupViewModel

    @Before
    fun init() {
        mSignupViewModel = SignupViewModel()
    }

    @Test
    fun onClickConfirmEmptyTest() {
        mSignupViewModel.onClickConfirm()

        Assert.assertEquals(false, mSignupViewModel.isSignupDone.get())
    }

    @Test
    fun onClickConfirmSuccessTest() {
        mSignupViewModel.cpf.set("15555541565116415")
        mSignupViewModel.email.set("teste@teste.com.br")
        mSignupViewModel.imei.set("15151551515151")
        mSignupViewModel.name.set("Teste")
        mSignupViewModel.phone.set("15151515116116")
        mSignupViewModel.password.set("teste")
        mSignupViewModel.confirmPassword.set("teste")

        mSignupViewModel.onClickConfirm()

        Assert.assertEquals(true, mSignupViewModel.isSignupDone.get())
    }

    @Test
    fun onClickConfirmFailTest() {
        mSignupViewModel.cpf.set("15555541565116415")
        mSignupViewModel.email.set("teste@teste.com.br")
        mSignupViewModel.imei.set("15151551515151")
        mSignupViewModel.name.set("Teste")
        mSignupViewModel.phone.set("15151515116116")
        mSignupViewModel.password.set("teste2")
        mSignupViewModel.confirmPassword.set("teste")

        mSignupViewModel.onClickConfirm()

        Assert.assertEquals(false, mSignupViewModel.isSignupDone.get())
        Assert.assertEquals(true, mSignupViewModel.isError.get())
    }
}