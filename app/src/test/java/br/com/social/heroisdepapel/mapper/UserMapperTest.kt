package br.com.social.heroisdepapel.mapper

import br.com.social.heroisdepapel.model.UserModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Testando a entidade construida para verificar se o mapeamento esta coerente para
 * repasse de dados
 */
class UserMapperTest {
    private lateinit var userMapper:UserMapper

    @Before
    fun init() {
        userMapper = UserMapper()
    }

    @Test
    fun transform() {
        var userModel = UserModel(
            id = 1,
            phone = "81999658463",
            name = "Teste",
            cpf = "05468243407",
            email = "teste@teste.com.br",
            imei = "1515454634663",
            password = "teste0546"
        )

        var user = userMapper.transform(userModel)

        Assert.assertEquals(user.id, userModel.id)
        Assert.assertEquals(user.phone, userModel.phone)
        Assert.assertEquals(user.name, userModel.name)
        Assert.assertEquals(user.cpf, userModel.cpf)
        Assert.assertEquals(user.email, userModel.email)
        Assert.assertEquals(user.imei, userModel.imei)
        Assert.assertEquals(user.password, userModel.password)
    }
}