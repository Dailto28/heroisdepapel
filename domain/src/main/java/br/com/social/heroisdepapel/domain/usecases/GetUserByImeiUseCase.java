package br.com.social.heroisdepapel.domain.usecases;

import br.com.social.heroisdepapel.domain.common.Result;
import br.com.social.heroisdepapel.domain.model.User;
import br.com.social.heroisdepapel.domain.repository.UserRepository;

/**
 * Caso de uso para retornar o usuario com base no imei
 */
public class GetUserByImeiUseCase extends UseCase<Result<User>, GetUserByImeiUseCase.Params> {

    private UserRepository userRepository;

    public GetUserByImeiUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    Result<User> build(Params params) {
        return userRepository.getUser(params.imei);
    }

    /**
     * Parametros usados para realizar uma requicao de retornar um usuario
     */
    public static final class Params {
        private final String imei;

        private Params(String imei) {
            this.imei = imei;
        }

        public static Params forUser(String imei) {
            return new Params(imei);
        }
    }
}
