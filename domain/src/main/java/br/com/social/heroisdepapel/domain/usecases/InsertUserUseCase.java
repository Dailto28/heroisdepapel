package br.com.social.heroisdepapel.domain.usecases;

import br.com.social.heroisdepapel.domain.common.Result;
import br.com.social.heroisdepapel.domain.model.User;
import br.com.social.heroisdepapel.domain.repository.UserRepository;

/**
 * Caso de uso para retornar o usuario com base no imei
 */
public class InsertUserUseCase extends UseCase<Result<User>, InsertUserUseCase.Params> {

    private UserRepository userRepository;

    public InsertUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    Result<User> build(Params params) {
        if (params == null) {
            return new Result.Error(new NullPointerException("Params can't null"));
        }

        return userRepository.insertUser(params.toUser());
    }

    public static final class Params {
        private final Long id;
        private final String phone;
        private final String name;
        private final String cpf;
        private final String email;
        private final String imei;
        private final String password;

        public Params(Long id, String phone, String name, String cpf, String email, String imei, String password) {
            this.id = id;
            this.phone = phone;
            this.name = name;
            this.cpf = cpf;
            this.email = email;
            this.imei = imei;
            this.password = password;
        }

        public static Params forUser(Long id, String phone, String name, String cpf, String email, String imei, String password) {
            return new Params(id, phone, name, cpf, email, imei, password);
        }

        private User toUser() {
            User user = new User();
            user.setId(id);
            user.setCpf(cpf);
            user.setEmail(email);
            user.setImei(imei);
            user.setName(name);
            user.setPassword(password);
            user.setPhone(phone);
            return user;
        }
    }
}
