package br.com.social.heroisdepapel.domain.common;

/**
 * Define os mapeadores para conversao de entidade, permitindo converter de um
 * tipo local de entidade para outro
 * @param <From> entidade inicial
 * @param <To> entidade final
 */
public interface Mapper<From, To> {

    /**
     * Converte uma entidade
     *
     * @param from entidade inicial
     * @return entidade final
     */
    To transform(From from);

    /**
     * Converte uma entidade de forma reversa
     *
     * @param from entidade inicial
     * @return entidade final
     */
    From reverse(To from);
}
