package br.com.social.heroisdepapel.domain.model;

/**
 * Dados do estabelecimento
 */
public class Establishment {

    /**
     * Nome do estabelecimento
     */
    private String name;

    /**
     * O CNPJ do estabelecimento para comprovacao
     */
    private String cnpj;

    /**
     * Nome do responsavel pelo estabelecimento
     */
    private String nomeResponsavel;

    /**
     * Endereco do estabelecimento
     */
    private String address;

    /**
     * A posicao no mapa na orientacao em latitude e logitude
     */
    private Position position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    public void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Establishment{" +
                "name='" + name + '\'' +
                ", cnpj='" + cnpj + '\'' +
                ", nomeResponsavel='" + nomeResponsavel + '\'' +
                ", address='" + address + '\'' +
                ", position=" + position +
                '}';
    }
}
