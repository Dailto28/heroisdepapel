package br.com.social.heroisdepapel.domain.repository;

import br.com.social.heroisdepapel.domain.common.Result;
import br.com.social.heroisdepapel.domain.model.User;

/**
 * Definicao de repositorio para captura de usuario
 */
public interface UserRepository {

    /**
     * Retorna um usuario atraves do imei do aparelho
     *
     * @param imei String identificador do usuario para cadastro
     * @return O Result com o estado sobre a busca
     */
    Result<User> getUser(String imei);

    /**
     * Insere um objeto do tipo User
     *
     * @param user Objeto usuario inserido na base do sistema
     * @return o mesmo usuario
     */
    Result<User> insertUser(User user);
}
