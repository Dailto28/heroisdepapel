package br.com.social.heroisdepapel.domain.usecases;

/**
 * Abstracao para criacao dos casos de uso usando com base do clean architecture
 *
 * @param <T>
 */
abstract class UseCase<T, Param> {

    abstract T build(Param param);

    public T execute(Param param) {
        return build(param);
    }
}
