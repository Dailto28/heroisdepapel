package br.com.social.heroisdepapel.domain.model;

import java.util.Date;

/**
 * Identicacao da mesa e dados do evento para poder
 * permitir realizar tal evento
 */
public class Table {

    /**
     * Nome da mesa
     */
    private String name;

    /**
     * Data marcada do evento
     */
    private Date date;

    /**
     * Numero de maximo de jogadores para o evento
     */
    private int maxNumberPlayers;

    /**
     * Comentario sobre o evento expondo explicacao ao mesmo
     */
    private String commentary;

    /**
     * Define se a mesma esta ativa ou inativa
     */
    private boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getMaxNumberPlayers() {
        return maxNumberPlayers;
    }

    public void setMaxNumberPlayers(int maxNumberPlayers) {
        this.maxNumberPlayers = maxNumberPlayers;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Table{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", maxNumberPlayers=" + maxNumberPlayers +
                ", commentary='" + commentary + '\'' +
                ", active=" + active +
                '}';
    }
}
