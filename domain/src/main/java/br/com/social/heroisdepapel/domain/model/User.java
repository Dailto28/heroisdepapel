package br.com.social.heroisdepapel.domain.model;

/**
 * Dados referente ao usuario, para que possa ser
 * identificado no sistema e para os seus devidos
 * perfis
 */
public class User {

    /**
     * Serve para identificar o usuario com base no id
     */
    private Long id;

    /**
     * Telefone de identificacao de captura do usuario
     * serve para realizar o acesso por base do numero de telefone
     */
    private String phone;

    /**
     * Nome do usuario que foi capturado para uso na aplicacao
     */
    private String name;

    /**
     * CPF de identificacao do usuario
     */
    private String cpf;

    /**
     * Email do usuario para identificacao do mesmo e retornar mensagem via email
     */
    private String email;

    /**
     * IMEI para identificar o aparelho
     */
    private String imei;

    /**
     * Senha para acesso do sistema para o caso de mudanca de aparelho
     */
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", imei='" + imei + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
