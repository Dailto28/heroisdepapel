package br.com.social.heroisdepapel.domain.usecases;

import br.com.social.heroisdepapel.domain.common.Result;
import br.com.social.heroisdepapel.domain.model.User;
import br.com.social.heroisdepapel.domain.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import static org.junit.Assert.assertTrue;

public class InsertUserUseCaseTest {

    private InsertUserUseCase insertUserUseCase;

    @Before
    public void init() {
        UserRepository userRepository = mock(UserRepository.class);

        when(userRepository.insertUser(null)).thenReturn(new Result.Error(new Exception()));
        when(userRepository.insertUser(any(User.class))).thenReturn(new Result.Success<User>(new User()));

        insertUserUseCase = new InsertUserUseCase(userRepository);
    }

    @Test
    public void executeFail() {
        assertTrue(insertUserUseCase.execute(null) instanceof Result.Error);
    }
}
