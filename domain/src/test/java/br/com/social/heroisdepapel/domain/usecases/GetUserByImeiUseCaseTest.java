package br.com.social.heroisdepapel.domain.usecases;

import br.com.social.heroisdepapel.domain.common.Result;
import br.com.social.heroisdepapel.domain.model.User;
import br.com.social.heroisdepapel.domain.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import static org.junit.Assert.assertTrue;

public class GetUserByImeiUseCaseTest {

    private GetUserByImeiUseCase getUserByImeiUseCase;

    @Before
    public void init() {

        UserRepository userRepository = mock(UserRepository.class);

        when(userRepository.getUser("126736273627362")).thenReturn(new Result.Error(new Exception()));
        when(userRepository.getUser("121212121212112")).thenReturn(new Result.Success<>(new User()));

        getUserByImeiUseCase = new GetUserByImeiUseCase(userRepository);
    }

    @Test
    public void executeErrorTest() {
        Result<User> execute = getUserByImeiUseCase.execute(GetUserByImeiUseCase.Params.forUser("126736273627362"));

        assertTrue(execute instanceof Result.Error);
    }

    @Test
    public void executeSuccessTest() {
        Result<User> execute = getUserByImeiUseCase.execute(GetUserByImeiUseCase.Params.forUser("121212121212112"));

        assertTrue(execute instanceof Result.Success);
    }
}
