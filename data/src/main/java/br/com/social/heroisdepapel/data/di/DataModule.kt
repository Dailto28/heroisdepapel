package br.com.social.heroisdepapel.data.di

import br.com.social.heroisdepapel.data.mapper.UserMapper
import br.com.social.heroisdepapel.data.model.MyObjectBox
import br.com.social.heroisdepapel.data.repository.UserDataRepository
import br.com.social.heroisdepapel.domain.repository.UserRepository
import br.com.social.heroisdepapel.domain.usecases.GetUserByImeiUseCase
import br.com.social.heroisdepapel.domain.usecases.InsertUserUseCase
import io.objectbox.BoxStore
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val dataMapper = module {
    single { UserMapper() }
}

val dataRepositoryModule = module {
    single<BoxStore> {
        MyObjectBox.builder()
            .androidContext(androidApplication())
            .build() }

    single<UserRepository> { UserDataRepository(boxStore = get(), userMapper = get())}
}

val dataUseCaseModule = module {
    single { GetUserByImeiUseCase(get()) }
    single { InsertUserUseCase(get()) }
}