package br.com.social.heroisdepapel.data.model

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

/**
 * Entidade usuario que sera persistida no sistema
 */
@Entity
data class UserData (
    @Id
    var id: Long,
    var phone: String,
    var name: String,
    var cpf: String,
    var email: String,
    var imei: String,
    var password: String
)