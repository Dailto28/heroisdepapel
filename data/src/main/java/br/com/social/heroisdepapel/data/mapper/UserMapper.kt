package br.com.social.heroisdepapel.data.mapper

import br.com.social.heroisdepapel.data.model.UserData
import br.com.social.heroisdepapel.domain.common.Mapper
import br.com.social.heroisdepapel.domain.model.User

class UserMapper : Mapper<UserData, User> {

    override fun transform(userModel: UserData): User {
        var result = User()
        result.id = userModel.id
        result.cpf = userModel.cpf
        result.email = userModel.email
        result.imei = userModel.imei
        result.name = userModel.name
        result.password = userModel.password
        result.phone = userModel.phone
        return result
    }

    override fun reverse(from: User): UserData = UserData(
        id = from.id,
        cpf = from.cpf,
        email = from.email,
        imei = from.imei,
        name = from.name,
        password = from.password,
        phone = from.phone
    )
}