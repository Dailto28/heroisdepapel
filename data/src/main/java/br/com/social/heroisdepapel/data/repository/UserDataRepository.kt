package br.com.social.heroisdepapel.data.repository

import android.content.res.Resources
import br.com.social.heroisdepapel.data.mapper.UserMapper
import br.com.social.heroisdepapel.data.model.UserData
import br.com.social.heroisdepapel.data.model.UserData_
import br.com.social.heroisdepapel.domain.common.Result
import br.com.social.heroisdepapel.domain.model.User
import br.com.social.heroisdepapel.domain.repository.UserRepository
import io.objectbox.BoxStore

class UserDataRepository (
    private val boxStore: BoxStore,
    private val userMapper: UserMapper
) : UserRepository {

    override fun getUser(imei: String?): Result<User> {

        val box = boxStore.boxFor(UserData::class.java)

        val query = box.query().run {
            equal(UserData_.imei, imei)
            build()
        }

        val findFirst = query.findFirst()

        if (findFirst != null) {
            val result: Result.Success<User> = Result.Success<User>(userMapper.transform(findFirst))

            return result as Result<User>
        }

        val result: Result.Error = Result.Error(Resources.NotFoundException("Entidade nao encontrada"));

        return result as Result<User>
    }

    override fun insertUser(user: User): Result<User> {

        val box = boxStore.boxFor(UserData::class.java)

        val reverse = userMapper.reverse(user)

        box.put(reverse)

        val result: Result.Success<User> = Result.Success<User>(userMapper.transform(reverse))

        return result as Result<User>
    }
}